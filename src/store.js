import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        techBlockImageId: "images-web-site"
    },
    actions: {
        setTechBlockImageId({ commit }, imageId) {
            commit("setTechBlockImageId", imageId);
        }
    },
    getters:{
        getTechBlockImageId: state => {
            return state.techBlockImageId
        }
    },
    mutations: {
        setTechBlockImageId(state, imageId) {
            state.techBlockImageId = imageId;
        }
    },
});